/*
Dealer Name: i.e.SmartSytems
System Name: IESS VoIP v1.1
System Number:
Programmer: Matthew Laletas
Comments: IESS VoIP v1.1
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#HELP "IESS - VoIP v1.1"
#HELP ""
#HELP "INPUTS:"
#HELP "StatusCall_Connected_FB: Feed this from the Codec module"
#HELP "StatusCall_Disconnected_FB: Feed this from the Codec module"
#HELP "Dial_Send_End: The toggle button from the TP, will determine whether to use DTMF or add to the DialString internally"
#HELP "Keypad_X: From the TP, will determine whether to use DTMF or add to the DialString internally"
#HELP ""
#HELP "OUTPUTS:"
#HELP "Dial_Send: Tie this into Dial command of the Codec module, the DialString will have the number that was entered"
#HELP "Dial_End: Tie this into the End command of the Codec module"
#HELP "DTMF_X: Tie this into the DTMF commands of the Codec module, this will be used depending on what state the StatusCall FB is"
#HELP "DialString: Tie this into the Dial string of the Codec module"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT StatusCall_Connected_FB, StatusCall_Disconnected_FB, _SKIP_;
DIGITAL_INPUT Dial_Send_End, _SKIP_, VoIP_PopUP, _SKIP_;
DIGITAL_INPUT Keypad_0, Keypad_1, Keypad_2, Keypad_3, Keypad_4, Keypad_5, Keypad_6, Keypad_7, Keypad_8, Keypad_9, Keypad_Star, Keypad_Pound, Keypad_Dot, Keypad_BackSpace_Clear, _SKIP_;
DIGITAL_INPUT Answer, Decline, _SKIP_, JoinCall, _SKIP_;

ANALOG_INPUT Call_State[6];
STRING_INPUT Call_State_Text[6][127], Call_State_CID_Name[6][127], Call_State_CID_Num[6][127];

DIGITAL_OUTPUT Dial_Send, Dial_End, _SKIP_, VoIP_PopUP_ON, _SKIP_;
DIGITAL_OUTPUT DTMF_0, DTMF_1, DTMF_2, DTMF_3, DTMF_4, DTMF_5, DTMF_6, DTMF_7, DTMF_8, DTMF_9, DTMF_Star, DTMF_Pound, DTMF_Dot, _SKIP_;
DIGITAL_OUTPUT AnswerCall, DeclineCall, _SKIP_;
STRING_OUTPUT DialString, _SKIP_;

ANALOG_OUTPUT Call_Selected, Call_Selected_State;
STRING_OUTPUT Call_Selected_State_Text, Call_Selected_State_CID_Name, Call_Selected_State_CID_Num;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER GLBL_PopUp, GLBL_CallSelected, GLBL_IncomingSelected, GLBL_CallActive[6];
STRING GLBL_DialString[6][50];
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION CallSelection( INTEGER lvIndex )
{
	GLBL_CallSelected = lvIndex;
	Call_Selected = lvIndex;
	Call_Selected_State = Call_State[lvIndex];
	Call_Selected_State_Text = Call_State_Text[lvIndex];
	Call_Selected_State_CID_Name = Call_State_CID_Name[lvIndex];
	Call_Selected_State_CID_Num = Call_State_CID_Num[lvIndex];
	DialString = GLBL_DialString[lvIndex];
}
FUNCTION ChangeCallInActiveSelection()
{
	INTEGER lvCounter, lvCall;
	lvCall = 0;
	FOR( lvCounter = 6 TO 1 STEP -1 )
	{
		IF( !GLBL_CallActive[lvCounter]	)
		{
			lvCall = lvCounter;
		}
	}
	IF( lvCall )
		CallSelection( lvCall );
	ELSE
		CallSelection( 1 );
}
FUNCTION ChangeCallActiveSelection()
{
	INTEGER lvCounter, lvCall;
	lvCall = 0;
	FOR( lvCounter = 1 TO 6 )
	{
		IF( GLBL_CallActive[lvCounter]	)
		{
			lvCall = lvCounter;
		}
	}
	IF( lvCall )
		CallSelection( lvCall );
	ELSE
		CallSelection( 1 );
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
PUSH Dial_Send_End
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, Dial_End );
	ELSE
		PULSE( 20, Dial_Send );
}	
PUSH VoIP_PopUP
{
	GLBL_PopUp = !GLBL_PopUp;
	VoIP_PopUP_ON = GLBL_PopUp;
}				
CHANGE StatusCall_Connected_FB
{
	IF( StatusCall_Connected_FB )
	{
		GLBL_CallActive[1] = 1;
	}
	ELSE
	{
		GLBL_CallActive[1] = 0;
		DialString = "";
		GLBL_DialString[1] = "";
	}
}
CHANGE StatusCall_Disconnected_FB
{
	IF( StatusCall_Disconnected_FB )
	{
		GLBL_CallActive[1] = 0;
		DialString = "";
		GLBL_DialString[1] = "";
	}
	ELSE
	{
		GLBL_CallActive[1] = 1;
	}
}
PUSH Keypad_0
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_0 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "0";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_1
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_1 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "1";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_2
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_2 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "2";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_3
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_3 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "3";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_4
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_4 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "4";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_5
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_5 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "5";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_6
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_6 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "6";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_7
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_7 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "7";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_8
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_8 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "8";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_9
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_9 );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "9";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_Star
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_Star );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "*";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_Pound
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_Pound );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + "#";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_Dot
{
	IF( GLBL_CallActive[GLBL_CallSelected] )
		PULSE( 20, DTMF_Dot );
	GLBL_DialString[GLBL_CallSelected] = GLBL_DialString[GLBL_CallSelected] + ".";
	DialString = GLBL_DialString[GLBL_CallSelected];
}
PUSH Keypad_BackSpace_Clear
{
	WAIT( 100, Clear )
	{
		IF( Keypad_BackSpace_Clear )
		{
			GLBL_DialString[GLBL_CallSelected] = "";
			DialString = GLBL_DialString[GLBL_CallSelected];
		}
	}
}
RELEASE Keypad_BackSpace_Clear
{
	STRING lvString[50];
	IF( LEN( GLBL_DialString[GLBL_CallSelected] ) > 0 )
	{
		lvString = GLBL_DialString[GLBL_CallSelected];
		lvString = REMOVEBYLENGTH( LEN( GLBL_DialString[GLBL_CallSelected] ) - 1, lvString );
		GLBL_DialString[GLBL_CallSelected] = lvString;
		DialString = GLBL_DialString[GLBL_CallSelected];
	}
}
PUSH JoinCall
{
	ChangeCallInActiveSelection();
}
CHANGE Call_State
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	IF( Call_State[lvIndex] = 4 || Call_State[lvIndex] = 6 || Call_State[lvIndex] = 7 || ( Call_State[lvIndex] >= 13 && Call_State[lvIndex] <= 18 ) )
	{
		GLBL_CallActive[lvIndex] = 1;
		GLBL_DialString[lvIndex] = "";
	}
	ELSE IF( Call_State[lvIndex] = 8 )
		GLBL_IncomingSelected = lvIndex;
	ELSE IF( Call_State[lvIndex] = 3 )
	{
		GLBL_CallActive[lvIndex] = 0;
		GLBL_DialString[lvIndex] = "";
		ChangeCallActiveSelection();
	}
	IF( lvIndex = GLBL_CallSelected )
	{
		Call_Selected_State = Call_State[lvIndex];
		DialString = GLBL_DialString[GLBL_CallSelected];
	}
}
CHANGE Call_State_Text
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
    IF( lvIndex = GLBL_CallSelected )
    	Call_Selected_State_Text = Call_State_Text[lvIndex];
}
CHANGE Call_State_CID_Name
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
    IF( lvIndex = GLBL_CallSelected )
    	Call_Selected_State_CID_Name = Call_State_CID_Name[lvIndex];
}
CHANGE Call_State_CID_Num
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
    IF( lvIndex = GLBL_CallSelected )
    	Call_Selected_State_CID_Num = Call_State_CID_Num[lvIndex];
}
PUSH Answer
{
	CallSelection( GLBL_IncomingSelected );
	PULSE( 20, AnswerCall );
}
PUSH Decline
{
	Call_Selected = GLBL_IncomingSelected;
	DeclineCall = 1;
	WAIT( 20 )
	{
		DeclineCall = 0;
		Call_Selected = GLBL_CallSelected;
	}
}
FUNCTION MAIN()
{
	CallSelection( 1 );
	Call_Selected = 1;
	GLBL_IncomingSelected = 1;
	GLBL_CallSelected = 1;
}
