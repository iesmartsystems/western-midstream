namespace HttpsUtility.Diagnostics;
        // class declarations
         class DebugLevel;
    static class DebugLevel // enum
    {
        static SIGNED_LONG_INTEGER Notice;
        static SIGNED_LONG_INTEGER Info;
        static SIGNED_LONG_INTEGER Warning;
        static SIGNED_LONG_INTEGER Error;
        static SIGNED_LONG_INTEGER Exception;
        static SIGNED_LONG_INTEGER All;
    };

namespace HttpsUtility.Https;
        // class declarations
         class HttpsClientPool;
         class HttpsResult;
     class HttpsClientPool 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Dispose ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace HttpsUtility.Symbols;
        // class declarations
         class SimplHttpsClient;
     class SimplHttpsClient 
    {
        // class delegates
        delegate FUNCTION SimplHttpsClientResponseDelegate ( INTEGER status , SIMPLSHARPSTRING responseUrl , SIMPLSHARPSTRING content , SIGNED_LONG_INTEGER contentLength );

        // class events

        // class functions
        INTEGER_FUNCTION SendGet ( STRING url , STRING headers );
        INTEGER_FUNCTION SendPost ( STRING url , STRING headers , STRING content );
        INTEGER_FUNCTION SendPut ( STRING url , STRING headers , STRING content );
        INTEGER_FUNCTION SendDelete ( STRING url , STRING headers , STRING content );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty SimplHttpsClientResponseDelegate SimplHttpsClientResponse;
    };

namespace HttpsUtility;
        // class declarations
         class Lazy;
     class Lazy 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

